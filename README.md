# Gitlab CICD Notes:

# Pipelines
- Made up of:
  - Jobs
    - Define what to do
    - executed by runners
    - Multiple jobs in the same stage are executed in parallel, if there are enough concurrent runners
  - Stages
    - Define when to run the jobs
- Types of Pipelines
  - `Basic`
    - run in stages
  - `Directed Acyclic Graph Pipeline (DAG)`
    - Based on relationships between jobs ('needs' syntax)
    - Can run more quickly than basic pipelines
  - `Merge Request`
    - Run for `merge requests` only
    - Do not run on `commits`
  - `Merged results`
    - These are merge resquest pipelines that act as though the changes from the source branch have already been merged into the target branch
  - `Merge trains`
    - Use merged results pipelines to queue merges one after the other
  - `Parent-child pipelines`
    - Breaks down complex pipelines into one parent pipeline that can trigger multiple child sub-pipelines
    - All sub-pipelines run in the same project and with the same SHA
    - Commonly used for mono-repos
  - `Multi-project pipelines`
    - Combine pipelines for different projects together